<?php


namespace app\index\controller;


use think\Controller;

class TcpClient extends Controller
{
    public function send()
    {
        // 实例化同步阻塞 TCP 客户端
        $client = new \swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_SYNC);

        // 建立连接，连接失败时停止程序
        if(!$client->connect('0.0.0.0',9508)){
            echo "connect failed"; die;
        }
        echo '连接成功';

        if($client->isConnected()){
            // echo "connect succesd";die;
            $sendStr = "\x03\x2f\x00\x00\x01\x11\x44";  // 16进制数据
            $client->send(str_replace(' ', '', $sendStr), 1);
        }else{
            echo "connect failed";die;
        }
        // 接收数据的最大长度为700，不等待所有数据到达后返回
        if($client->recv()){
            $data = $client->recv(700);
        }else{
            $data = $client->errorCode;
        }
        echo "recv: {$data} \n";
        // print_r($client);die;
    }
}