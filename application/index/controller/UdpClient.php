<?php


namespace app\index\controller;

use think\Controller;

class UdpClient extends Controller
{
    public function send()
    {
        // 实例化同步阻塞 UDP 客户端
        $client = new \swoole_client(SWOOLE_SOCK_UDP, SWOOLE_SOCK_SYNC);

        // 建立连接，连接失败时停止程序
        $client->connect('127.0.0.1', 9502) or die("connect failed\n");

        // 向 UDP 服务器发送数据
        $client->send('111');
        echo "ok";
    }

}