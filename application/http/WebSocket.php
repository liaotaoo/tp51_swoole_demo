<?php
namespace app\http;

use think\console\Command;
use think\console\Input;
use think\console\Output;

/**
 * Class WebSocket
 * @package app\http
 * 启动: php think websocket:start
 */
class WebSocket extends Command
{
    // Server 实例
    protected $server;

    //tp5 命令行配置详细用法 https://blog.csdn.net/kelinfeng16/article/details/88549717
    protected function configure()
    {
        //设置启动命令
        //可在项目里面 使用 php think 查看所有命令

        $this->setName('websocket:start')->setDescription('Start Web Socket Server!');
    }

    protected function execute(Input $input, Output $output)
    {
        // 监听所有地址，监听 10000 端口
        $this->server = new \swoole_websocket_server('0.0.0.0',9508);
        
        // 设置 server 运行前各项参数
        // 调试的时候把守护进程关闭，部署到生产环境时再把注释取消
        // $this->server->set([
        //     'daemonize' => true,
        // ]);
        
        // 设置回调函数
        $this->server->on('Open', [$this, 'onOpen']);
        $this->server->on('Message', [$this, 'onMessage']);
        $this->server->on('Close', [$this, 'onClose']);
        
        $this->server->start();
        // $output->writeln("WebSocket: Start.\n");
    }

    // 建立连接时回调函数
    public function onOpen(\swoole_websocket_server $server, \swoole_http_request $request)
    {
        echo "server: handshake success with fd{$request->fd}\n";
    }

    // 收到数据时回调函数
    public function onMessage(\swoole_websocket_server $server, \swoole_websocket_frame $frame)
    {
        echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
        $server->push($frame->fd, "this is server");
    }

    // 连接关闭时回调函数
    public function onClose($server, $fd)
    {
        echo "client {$fd} closed\n";
    }
}