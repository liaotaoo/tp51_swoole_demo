<?php
namespace app\http;

use think\swoole\Server;

/**
 * Class Swoole
 * @package app\http
 *  此类属于官方文档自定义类方式
 *  此方式需要在swoole_server.php 添加配置  'swoole_class' => 'app\http\Swoole',
 *  cli 启动    :    php think swoole:server
 *  tp5.1官方文档相同
 */
class Swoole extends Server
{
    protected static $token;
    protected $host = '0.0.0.0';
    protected $port = 9508;
    protected $serverType = 'socket';

    protected $option = [
        'worker_num' => 4, // 设置启动的Worker进程数
        'daemonize' => false, //是否守护进程。
        'backlog' => 128, //Listen队列长度，
        'dispatch_mode' => 2,
        // 'heartbeat_check_interval' => 5, // 延时
        // 'heartbeat_idle_time' => 100, // 心跳
    ];

    /**
     * 收到信息时回调函数
     * @param \swoole_server $server swoole_server对象
     * @param $fd TCP客户端连接的文件描述符
     * @param $from_id TCP连接所在的Reactor线程ID
     * @param $data 收到的数据内容
     */
    public function onReceive($server, $fd, $from_id, $data)
    {
        echo '发现数据';
        $server->task($data);
    }

    public function onWorkerStart($server, $req)
    {

    }

    public function onOpen( $server,  $req)
    {
        echo $req->fd.'连接成功';


    }

    public function onTask($server,$task_id,$from_id, $data)
    {

    }

    public function onStart( $server )
    {
        echo "Start\n";
        //      Swoole::tick(3000, function () {
        //     echo "after 3000ms.\n";
        //     Swoole::tick(14000, function () {
        //         echo "after 14000ms.\n";
        //     });
        // });
        $i = 0;
        swoole_timer_tick(2000, function ($timer_id) use (&$i){
            // echo "{$timer_id}-tick-2000ms\n";
            $i++;
            echo $i."\n";
            if($i == 10){
                echo '已经执行了十次定时器方法了';
                swoole_timer_clear($timer_id);
            }
        });

    }

    public function onConnect($server, $fd)
    {
        echo "Connected---$fd\n";
    }


    public function onMessage($server, $frame)
    {
        // var_dump($frame);
        $data = json_decode($frame->data,true);

        switch($data['type']){
            case 'xiaohu':
                $server->push($frame->fd, json_encode(['type'=>'say1','message'=>"hello,小虎收到收到，有屁就放，大哥忙的横"],true));
                var_dump(json_encode(['type'=>'say1','message'=>"hello,小虎收到收到，有屁就放，大哥忙的横"],true));
                break;
            case 'xiaohusay':
                $server->push($frame->fd, json_encode(['type'=>'init','message'=>"NMMP,你脑袋才有病，SB"],true));
                break;
            case 'haha':
                $server->push($frame->fd, json_encode(['type'=>'haha','message'=>"爪子嘛"],true));
                break;
            case 'reload':
                $server->push($frame->fd, json_encode(['type'=>'reload','message'=>"收到重启请求,正在重启"],true));
                $server->reload($only_reload_taskworkrer = false);
                break;
            default:
                foreach ($server->connections as $fd){
                    $server->push($fd,json_encode(['type'=>'all','message'=>$fd.'all']));
                }

        }

    }
    function onClose($server, int $fd, int $reactorId)
    {
        echo "Client +fd:{$fd}+ +reactorId:{$reactorId}+close connection\n";
    }

}