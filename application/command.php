<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

return [
    'app\http\WebSocket',
    'app\http\TcpServer',
    'app\http\TcpClient',
    'app\http\AsyncTcpClient',
    'app\http\UdpServer',
    'app\http\AsyncTask',
    'app\http\Timer',
];